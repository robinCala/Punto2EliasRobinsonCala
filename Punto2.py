# CODIGO : Descripcion, precio, Stock

from os import system

def validarCodigo(diccionario):
    while True:
        try:
            codigo = int(input('Ingrese Codigo (unico) para su Producto: '))
            resultado = True
            while resultado == True:
                resultado = False
                for cod,datos in diccionario.items():
                    if cod == codigo:
                        resultado = True
                        codigo = int(input('El Codigo Ingresado YA EXISTE\nIngrese otro codigo (debe ser unico):'))
                        break   
            break
        except:
            print('EL VALOR INGRESADO DEBE SER NUMERICO\n\n')   
    return (codigo)

def validarCampo():
    campo = input('Ingrese DESCRIPCION del Producto:')
    while (len(campo) <= 0):
        campo = input('Campo OBLIGATORIO *\nIngrese Descripcion del Producto:')
    return (campo)

def validarPrecio():
    while True:
        try:
            numero = float(input('Ingrese PRECIO del Producto:'))
            while (numero <= 0):
                numero = input('Campo OBLIGATORIO * (el precio NO puede ser CERO o un valor MENOR a este)\nIngrese PRECIO del Producto:')
                print('\n')
            break
        except:
            print('EL VALOR INGRESADO DEBE SER NUMERICO\n\n')   
    return (numero)

def validarStock():
    while True:
        try:
            numero = int(input('Ingrese STOCK del Producto:'))
            while (numero <= 0):
                numero = input('Campo OBLIGATORIO * (el Stock NO puede ser CERO o un valor MENOR a este)\nIngrese STOCK del Producto:')
                print('\n')
            break
        except:
            print('EL VALOR INGRESADO DEBE SER NUMERICO y ENTERO\n\n')
    return (numero)

def registrarProducto(diccionario):
    codigo = validarCodigo(diccionario)
    descripción = validarCampo()
    precio = validarPrecio()
    stock = validarStock()
    diccionario[codigo] = [descripción,precio,stock]

def validarNumero(palabra):
    while True:
        try:
            numero = int(input(palabra))
            while (numero <= 0):
                numero = input('Campo OBLIGATORIO * (el Numero NO puede ser CERO o un valor MENOR a este)\n',palabra)
                print('\n')
            break
        except:
            print('EL VALOR INGRESADO DEBE SER NUMERICO y ENTERO\n\n')
    return (numero)

def mostrarStockIntervalo(desde,hasta,diccionario):
    print('Se Mostraran los productos cuyos Stock esten desde:',desde,'   hasta:',hasta)
    for cod,valores in diccionario.items():
        if (valores[2] >= desde and valores[2] <= hasta):
            print('Codigo: ',cod,'   Descripcion: ',valores[0],'   Precio: ',valores[1],'   Stock: ',valores[2])

def sumarX_Menor(x,y,diccionario):
    print('Se sumara:',x,' en Stock a todos los productos con el Stock menor a:',y)
    for cod,valores in diccionario.items():
        if (valores[2] < y):
            valores[2] += x

def eliminarStockCero(diccionario):
    i = 0
    while (i <= len(diccionario) ):
        for cod,valores in diccionario.items():
            if valores[2] == 0:
                del diccionario[cod]
                break
        i += 1
    
def mostrarProductos(diccionario):
    for cod,valores in diccionario.items():
        print('Codigo: ',cod,'   Descripcion: ',valores[0],'   Precio: ',valores[1],'   Stock: ',valores[2])

def menu():
    print('1__Registrar Productos')
    print('2__Mostrar listado de Productos')
    print('3__Mostrar los productos cuyo stock se encuentre en el intervalo [desde, hasta]')
    print('4__Sumar "x" de Stock a los Productos cuyo Stock sea menor a "y"')
    print('5__Eliminar todos los productos cuyo stock sea igual a cero')
    print('0__SALIR')
    print()
    while True:
        seleccion = int(input('Seleccione alguno: '))
        if (seleccion >= 0 and seleccion <= 9):
            break
    return (seleccion)

def continuar():
    input('\nPress Enter to Continue...')
    system('CLS')
    
  
#PROGRAMA PRINCIPAL

system('cls')


productos = {
    100:['Altavoces', 149.4, 0],
    101:['Televisor', 120.4, 7],
    102:['Celulares', 98.43, 15],
    103:['Teclados', 15.26, 3],
    104:['Altavoces', 149.4, 0],
    105:['Monitores', 200.39, 2],
    106:['Altavoces', 149.4, 0],
    107:['Altavoces', 149.4, 0]

}

while True:
    selec = menu()
    if selec == 1:
        registrarProducto(productos)
        continuar()
    if selec == 2:
        mostrarProductos(productos)
        continuar()
    if selec == 3:
        desde = validarNumero(palabra = 'Ingrese NUMERO de INCIO :')
        hasta = validarNumero(palabra = 'Ingrese NUMERO de FINALIZACION :')
        mostrarStockIntervalo(desde,hasta,productos)
        continuar()
    if selec == 4:
        x = validarNumero(palabra = 'Ingrese NUMERO que se sumara en los STOCKS: ')
        y = validarNumero(palabra = 'Ingrese NUMERO al cual los Stocks deben ser Menores: ')
        sumarX_Menor(x,y,productos)
        continuar()
    if selec == 5:
        eliminarStockCero(productos)
        continuar()
    if selec == 0:
        break
  
